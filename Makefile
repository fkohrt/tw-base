WIKI_DIR := shared

.PHONY: all clean

all: $(WIKI_DIR)/tiddlers/$$__toc-generic.json $(WIKI_DIR)/tiddlers/$$__trashbin-mod.json $(WIKI_DIR)/tiddlers/$$__plainrevs.json $(WIKI_DIR)/tiddlers/$$__focus.tid $(WIKI_DIR)/tiddlers/$$__plugins_linonetwo_opened-tiddlers-bar.json $(WIKI_DIR)/tiddlers/$$__plugins_linonetwo_opened-tiddlers-bar.json.meta

clean:
	rm -rf temp/
	rm $(WIKI_DIR)/tiddlers/\$$__*

$(WIKI_DIR)/tiddlers/$$__toc-generic.json: modules/HC-Plugins/index.html
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/HCHaase/TOCgeneric '$(@F)' text/plain $$:/core/templates/json-tiddler

$(WIKI_DIR)/tiddlers/$$__trashbin-mod.json: modules/HC-Plugins/index.html
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/hchaase/trashbin-mod '$(@F)' text/plain $$:/core/templates/json-tiddler

$(WIKI_DIR)/tiddlers/$$__plainrevs.json: temp/j-d-revisions.html
	tiddlywiki --load $< --output $(WIKI_DIR)/tiddlers/ --render $$:/plugins/jd/plainrevs '$(@F)' text/plain $$:/core/templates/json-tiddler

temp/j-d-revisions.html: temp/
	wget -O $@ http://j.d.revisions.tiddlyspot.com/

$(WIKI_DIR)/tiddlers/$$__focus.tid: modules/tiddlywiki-stuff/tiddlers/$$__focus.tid
	cp '$<' '$@'

$(WIKI_DIR)/tiddlers/$$__plugins_linonetwo_opened-tiddlers-bar.json: modules/wiki/tiddlers/$$__plugins_linonetwo_opened-tiddlers-bar.json
	cp '$<' '$@'

$(WIKI_DIR)/tiddlers/$$__plugins_linonetwo_opened-tiddlers-bar.json.meta: modules/wiki/tiddlers/$$__plugins_linonetwo_opened-tiddlers-bar.json.meta
	cp '$<' '$@'

temp/:
	mkdir temp/

