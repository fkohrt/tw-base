# TiddlyWiki Base

This TiddlyWiki base combines some basic plugins and settings in one place, ready to be imported by another wiki.

## Usage

- `git clone --recurse-submodules https://gitlab.com/fkohrt/tw-base`
- `tiddlywiki wiki/ --init empty`
- add `tw-base/shared/` as included wiki to `wiki/tiddlywiki.info`:
```
    "includeWikis": [
        {
            "path": "../tw-base/shared/",
            "read-only": "true"
        }
    ],
```
- (optional) add your language to `wiki/tiddlywiki.info`:
```
    "languages": [
        "de-DE"
    ],
```
- add your tiddlers to `wiki/tiddlers/`
- run `make` inside `tw-base/`

### Option 1: Static wiki

- build with `tiddlywiki wiki/ --output public/ --build index`
- created gzipped version of the static files: `find public/ -type f -exec gzip -f -k {} \;`

### Option 2: Dynamic wiki

#### Prepare folder

```bash
# settings
USER_HOME=/home/user
TW_PATH=$USER_HOME/Software/tiddlywiki/wiki

mkdir $TW_PATH/tmp/
mv $TW_PATH/tiddlers/*\$__rev* $TW_PATH/tmp/
mv $TW_PATH/tiddlers/*\$__trashbin* $TW_PATH/tmp/
rm $TW_PATH/tiddlers/*\$__*
cp $TW_PATH/config/* $TW_PATH/tiddlers/
cp $TW_PATH/tmp/* $TW_PATH/tiddlers/
rm -r -f $TW_PATH/tmp/
```

#### Option 2A: Running with TiddlyWiki's `--listen` command

```bash
# ... prepare folder (as above) ...

tiddlywiki $TW_PATH/ --listen host=0.0.0.0 port=8080 "root-tiddler=$:/core/save/all-external-js"
```

#### Option 2B: Running with BobWiki's `--wsserver` command

```bash
# ... prepare folder (as above) ...

# settings
SHARED_PATH=$USER_HOME/Software/tiddlywiki/shared
GIT_PATH=$USER_HOME/Software/TiddlyWiki5/

# copy from shared manually b/c BobWiki doesn't understand the "includeWikis" setting
cp $SHARED_PATH/tiddlers/* $TW_PATH/tiddlers/
rm -r -f $TW_PATH/plugins/
mkdir $TW_PATH/plugins/
cp -rL $SHARED_PATH/plugins/* $TW_PATH/plugins/

# adjust TW5-Bob settings
patch --forward --reject-file=- /home/user/Software/TiddlyWiki5/Wikis/BobWiki/settings/settings.json <<'EOF'
--- -
+++ /home/user/Software/TiddlyWiki5/Wikis/BobWiki/settings/settings.json
@@ -8,7 +8,7 @@
   },
   "ws-server": {
     "port": 8080,
-    "host": "127.0.0.1",
+    "host": "0.0.0.0",
     "autoIncrementPort": false
   },
   "heartbeat": {
EOF

node $GIT_PATH/tiddlywiki.js $GIT_PATH/Wikis/BobWiki --wsserver
```

## License

This base wiki is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) and thus in the public domain. The author hereby waived all copyright and related or neighboring rights together with all associated claims and causes of action with respect to this work to the extent possible under the law.

Third party software in use is licensed differently, as indicated by the following table:

Name|Author|License
--|--|--
[TiddlyWiki](https://tiddlywiki.com/)|Jeremy Ruston|BSD-3-Clause
[Context Search](https://github.com/danielo515/TW5-contextPlugin)|Daniel Rodriguez Rivero|BSD-3-Clause
[TiddlyMap](https://github.com/felixhayashi/TW5-TiddlyMap)|Felix Küppers|BSD-2-Clause
[TW5-Vis.js](https://github.com/felixhayashi/TW5-Vis.js)|Felix Küppers|BSD-2-Clause (vis.js library: Apache-2.0, MIT)
[HotZone](https://github.com/felixhayashi/TW5-HotZone)|Felix Küppers|BSD-2-Clause
[TopStoryView](https://github.com/felixhayashi/TW5-TopStoryView)|Felix Küppers|BSD-2-Clause
[Shiraz](https://github.com/kookma/TW-Shiraz)|Mohammad Rahmani|MIT (Bootstrap: MIT)
[TOCgeneric](https://hchaase.github.io/HC-Plugins/#%24%3A%2Fplugins%2FHCHaase%2FTOCgeneric)|Hans Christian Haase|BSD-3-Clause
[Trashbin](https://github.com/kookma/TW-Trashbin)|Mohammad Rahmani|MIT
[Trashbin-mod](https://hchaase.github.io/HC-Plugins/#%24%3A%2Fplugins%2Fhchaase%2Ftrashbin-mod)|Hans Christian Haase|Unlicense
[Locator](https://gitlab.com/bimlas/tw5-locator)|Bimba László|MIT
[Plain Revisions](http://j.d.revisions.tiddlyspot.com/#%24%3A%2Fplugins%2Fjd%2Fplainrevs)|jd|MIT
[Focus](https://fkohrt.gitlab.io/tiddlywiki-stuff/#%24%3A%2F.tb%2Fui%2Fstyles%2Ffocus)|Tobias Beer|CC0-1.0
[Tidgraph](https://ihm4u.github.io/tw5plugs/#%24%3A%2Fplugins%2Fihm%2Ftidgraph)|ihm4u|MIT
[Toggle story](https://fkohrt.gitlab.io/tiddlywiki-stuff/#%24%3A%2Fplugins%2Ffk%2Ftoggle-story)|Florian Kohrt|CC0-1.0
[Relink and Relink Markdown](https://github.com/flibbles/tw5-relink)|Cameron Fischer|BSD-3-Clause
[Opened Tiddlers Bar](https://github.com/tiddly-gittly/TiddlyGit-Desktop/tree/master/template/wiki/plugins/linonetwo/opened-tiddlers-bar)|Jeffrey Wikinson|MIT
[Crumbs](https://fkohrt.gitlab.io/tiddlywiki-stuff/#%24%3A%2Fplugins%2Fmatabele%2Fcrumbs)|William Jackson|

